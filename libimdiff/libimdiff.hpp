#ifndef LIBIMDIFF_HPP_
#define LIBIMDIFF_HPP_

#include <opencv2/core/mat.hpp>

namespace imdiff
{
    namespace utils
    {
        /*!
         * \brief Invokes callback with each 2 choose
         *
         * Lazily generates 2 choose vec.size() and invokes
         * callback on each step
         *
         * \todo Allow other containers
         */
        template<typename T, typename Cb>
        auto do_on_combination(const std::vector<T>& vec, Cb&& callback)
            -> std::enable_if_t<std::is_invocable_v<Cb&&, const T&, const T&>,
                void>
        {
            std::vector<bool> bitmask(2, true);
            bitmask.resize(vec.size(), false);

            // print integers and permute bitmask
            do {
                std::array<std::size_t, 2> choose_indices = {};
                auto it = choose_indices.begin();

                for (std::size_t mask_i = 0; mask_i < vec.size(); ++mask_i)
                    if (bitmask[mask_i]) {
                        *it = mask_i;
                        ++it;
                    }

                auto first_idx  = choose_indices[0],
                     second_idx = choose_indices[1];

                const auto& first = vec[first_idx];
                const auto& second = vec[second_idx];

                std::forward<Cb>(callback)(first, second);
            } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
        }
	} // namespace utils

    /*!
     * \brief Compares two image
     *
     * \arg The first image to compare
     * \arg The second image to compare
     *
     * \returns Image similarity in [0; 1] range
     *
     * \todo Replace raw double with special class
     * which will represent percents or probability
     */
    double similarity(const cv::Mat& first, const cv::Mat& second);
} // namespace imdiff

#endif // LIBIMDIFF_HPP_
