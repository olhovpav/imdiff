#include <libimdiff.hpp>

#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>

namespace imdiff::utils
{
    /*
    * \brief Converts image to HSV channels representation
    *
    * \return Returns the same image with HSV channels
    */
    cv::Mat to_hsv(cv::Mat image_copy) {
        cv::cvtColor(image_copy, image_copy, cv::ColorConversionCodes::COLOR_BGR2HSV);
        return image_copy;
    }
} // namespace imdiff::utils

namespace imdiff
{
    /*
    * \brief Calculates histogram
    *
    * Calculates HSV colour histogram of image and normalize it
    *
    * \return Returns normalize histogram
    */
    cv::Mat calculate_hist(const cv::Mat& image)
    {
        constexpr bool uniform = true;
        constexpr bool accumulate = false;
        constexpr std::array channels = { 0, 1 };
        const cv::Mat no_mask;
        constexpr int two_dims = 2;

        // 50x60 histogram size
        constexpr std::array hist_size = { 50, 60 };
        constexpr std::array hue_value_range = { 0.f, 180.f };
        constexpr std::array saturation_value_range = { 0.f, 256.f };
        std::array ranges = { hue_value_range.data(), saturation_value_range.data() };

        cv::Mat result;

        cv::calcHist(
            &image,
            channels.size(),
            channels.data(),
            no_mask,
            result,
            two_dims,
            hist_size.data(),
            ranges.data(),
            uniform,
            accumulate
        );
        cv::normalize(result, result, 0, 1, cv::NormTypes::NORM_MINMAX);

        return result;
    }
} // namespace imdiff

/*
 * \todo implement function body
 */
double imdiff::similarity(const cv::Mat& first, const cv::Mat& second)
{
    std::pair images = {
        utils::to_hsv(first),
        utils::to_hsv(second),
    };

    std::pair histograms = {
        imdiff::calculate_hist(images.first),
        imdiff::calculate_hist(images.second),
    };

    auto similarity = compareHist(histograms.first, histograms.second, cv::HistCompMethods::HISTCMP_CORREL);

    return similarity;
}
