#include <libimdiff.hpp>

#include <filesystem>
#include <iostream>

#include <vector>
#include <string>

#include <opencv2/imgcodecs.hpp>

namespace fs = std::filesystem;

bool is_valid_percent(double threshold)
{
    return 0.0 <= threshold && threshold <= 100.0;
}

std::vector<fs::path> read_paths()
{
    std::vector<fs::path> result;

    for (std::string as_string; std::getline(std::cin, as_string); )
    {
        // just ignore empty lines to improve UX
        if (as_string.empty()) continue;

        result.emplace_back(std::move(as_string));
    }

    return result;
}

cv::Mat open_image(const fs::path& image_location)
{
    if (!fs::exists(image_location))
        throw std::runtime_error{ "Invalid image location: " + image_location.string() };

    auto image = cv::imread(image_location.string());

    if (image.empty())
        throw std::runtime_error{ "Can't open image: " + image_location.string() };

    return image;
}

int main()
{
    double threshold = 0;
    std::cin >> threshold;

    if (! std::cin) {
        std::cerr << "Invalid input: percent should be valid floating point number" << std::endl;
        return 1;
    }

    if (!is_valid_percent(threshold)) {
        std::cerr << "Invalid input: percent should be in range [0; 100] %" << std::endl;
        return 1;
    }

    auto paths = read_paths();
    imdiff::utils::do_on_combination(
        paths,
        [threshold](const fs::path& first_image, const fs::path& second_image)
        {
            auto similarity = imdiff::similarity(open_image(first_image), open_image(second_image));

            if (similarity <= threshold / 100.0)
                return;

            std::cout << first_image << ", "
                      << second_image << ", "
                      // don't print fractional part
                      << static_cast<int>(std::round(similarity * 100)) << "%"
                      << std::endl;
        }
    );
}
