Imdiff
------

A simple tool to compare image similarity

It uses HSV image representation and histogram comparison to compare all images with each other. In a future we may implement template or feature matching 

Building
--------

I am using bash as my shell, so this instruction assumes you use bash or git-bash too. But you are free to choose other shells and use the instruction just as a reference

Clone OpenCV:
```bash
mkdir -p C:/Dev/Libs/Src/ C:/Dev/Libs/Build/
cd C:/Dev/Libs/
git clone https://github.com/opencv/opencv.git OpenCV
cd OpenCV
git worktree add ../OpenCV-4.5.4/ 4.5.4
```

Build OpenCV Release configuration
```bash
cd C:/Dev/Libs/

cmake -S ./Src/OpenCV-4.5.4/     \
      -B ./Build/OpenCV-4.5.4/   \
      -DCMAKE_BUILD_TYPE=Release \
      -DBUILD_TESTS=OFF          \
      -DBUILD_DOCS=OFF           \
      -DBUILD_EXAMPLES=ON

cmake --build ./Build/OpenCV-4.5.4/ \
      --config Release              \
      --parallel

cmake --install ./Build/OpenCV-4.5.4/ \
      --prefix  ./OpenCV-4.5.4/       \
      --config Release
```

Build OpenCV Debug configuration
```bash
cd C:/Dev/Libs/

cmake -S ./Src/OpenCV-4.5.4/     \
      -B ./Build/OpenCV-4.5.4/   \
      -DCMAKE_BUILD_TYPE=Debug   \
      -DBUILD_TESTS=OFF          \
      -DBUILD_DOCS=OFF           \
      -DBUILD_EXAMPLES=ON

cmake --build ./Build/OpenCV-4.5.4/ \
      --config Debug                \
      --parallel

cmake --install ./Build/OpenCV-4.5.4/ \
      --prefix  ./OpenCV-4.5.4/       \
      --config Debug
```


Clone Imdiff repository
```bash
cd C:/Dev/
git clone git@gitlab.com:olhovpav/imdiff.git Imdiff
```

Build source code:
```bash
cd C:/Dev/Imdiff
cmake -S ./                                    \
      -B ./Build/                                \
      -D OpenCV_ROOT=C:/Dev/Libs/OpenCV-4.5.4/ \
      -D CMAKE_BUILD_TYPE=Debug

cmake --build ./Build/ --config Debug

cmake --install ./Build/   \
      --prefix  ./Install/ \
      --config Debug
```

Add OpenCV libraries to PATH enviroment variable:
```bash
export PATH="$PATH:C:/Dev/Libs/OpenCV-4.5.4/x64/vc17/bin"
```

Run application:
```bash
cd ./Install/share/examples/
cat input.txt | ../../bin/imdiff.exe
```
Output:
```bash
"sample_1.png", "sample_2.png", 100%
"sample_1.png", "sample_3.png", 2%
"sample_2.png", "sample_3.png", 2%
```
